document.querySelector('button').addEventListener('click', function(evento){

    //API de IPCA do BC

    const URL = 'https://api.bcb.gov.br/dados/serie/bcdata.sgs.4448/dados?formato=json&dataInicial=20201101';

    fetch(URL).then(function(resultado){

        return resultado.text();

    }).then(function(ret){
        
        var retJson = JSON.parse(ret);

        console.log(retJson[357]['valor']);
    
        var json = {
          element: 'graficoIpca',
          data: [   { month: '2021-06', value: retJson[353]['valor'] },
                { month: '2021-07', value: retJson[354]['valor'] },
                { month: '2021-08', value: retJson[355]['valor'] },
                { month: '2021-09', value: retJson[356]['valor'] },
                { month: '2021-10', value: retJson[357]['valor'] }
              ],
          xkey: 'month',
          ykeys: ['value'],
          labels: ['IPCA']
        };
    
        Morris.Line(json);

    }).catch(function(){

        alert('API fora do ar');
    });   
});